" Leader
let mapleader="`"

source $VIMDIR/bundles.vim
source $VIMDIR/plugin_settings.vim
source $VIMDIR/settings.vim
source $VIMDIR/mappings.vim
