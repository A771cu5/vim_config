" General settings
syntax enable
filetype indent on
filetype plugin on
set splitright
set autoread
set scrolloff=4
set sidescrolloff=5
set ttyfast
set noerrorbells
set noeol
set relativenumber
set number
set nowrap
set smartcase
set smartindent
set backspace=indent,eol,start
set autoindent
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set regexpengine=1
set completeopt-=preview

" Theme
" colorscheme molokai
colorscheme monokai
hi NonText ctermbg=none
hi Normal guibg=NONE ctermbg=NONE
