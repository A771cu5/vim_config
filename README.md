# README #

### What do you need? ###

The environment variable VIMDIR is used for determining the installation location of the vim configuration.  If you have VIMDIR set already, make sure the location it refers to is available.  If VIMDIR is not already set, the script will use a default location of $HOME/.vim for the installation path.  Just like above, make sure this path is available if you want to use the default location for vim configuration.

### How do I get set up? ###

Run the following commands to setup your config:

```
#!bash

curl https://bitbucket.org/A771cu5/vim_config/raw/master/install.sh -o install.sh
source install.sh
```

It's important to note that you can't use ./install.sh to run the install script because it tries to assign environment variables if needed, which can't be done from a sub-shell. Using 'source install.sh' or '. install.sh' is required because it uses your current shell.

Or you can copy and paste the following one line into your shell:

```
#!bash

curl https://bitbucket.org/A771cu5/vim_config/raw/master/install.sh -o install.sh && source install.sh && rm install.sh
```
